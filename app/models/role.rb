class Role < ActiveRecord::Base
  has_and_belongs_to_many :users, :join_table => :users_roles
  belongs_to :resource, :polymorphic => true
  validates_uniqueness_of :name

  scopify

  def change_name
    case self.name
    when 'admin'
      I18n.t('role.admin')
    when 'events'
      I18n.t('role.events')
    when 'reception'
      I18n.t('role.reception')
    end
  end
end
