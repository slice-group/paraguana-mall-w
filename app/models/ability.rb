class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.has_role? :admin

      # - user authorize -
      can [:delete, :show, :edit, :update, :create, :index, :destroy_multiple], User
      can :destroy, User do |u| !u.eql?(user) end

      can :manage, Room
      can [:show, :edit, :update, :index, :statistics], Poll
      can [:index, :show], Result
      can :manage, Question
      can :manage, Promo
      can [:delete, :destroy, :show, :index, :destroy_multiple, :export], Client
      can [:delete, :destroy, :show, :index, :destroy_multiple, :export], Event
      can :manage, EventImage
    elsif user.has_role? :reception
      can :manage, Room
      can :manage, Promo
      can [:destroy, :show, :index, :export], Client
    elsif user.has_role? :events
      can :manage, Promo
      can [:delete, :destroy, :show, :index, :destroy_multiple, :export], Event
      can [:destroy, :show, :index, :export ], Client
      can :manage, EventImage
    end
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
