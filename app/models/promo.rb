#Generado por keppler
require 'elasticsearch/model'
class Promo < ActiveRecord::Base
  require 'file_size_validator'
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  before_save :set_public
  mount_uploader :image, PromoUploader
  validates_presence_of :name, :image
  validates_uniqueness_of :name

  after_commit on: [:update] do
    __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      image:  self.image.to_s,
      name:  self.name.to_s,
      public:  self.public.to_s,
    }.as_json
  end

  def set_public
    Promo.update_all(public: false) if self.public
  end
end
#Promo.import
