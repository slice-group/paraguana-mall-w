#Generado por keppler
require 'elasticsearch/model'
class Result < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  belongs_to :poll
  has_many :results_questions, dependent: :destroy

  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      firstname:  self.firstname.to_s,
      lastname:  self.lastname.to_s,
      email:  self.email.to_s,
      poll_id:  self.poll_id.to_s,
    }.as_json
  end

  def fullname
    "#{firstname} #{lastname}"
  end

end
#Result.import
