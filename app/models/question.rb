class Question < ActiveRecord::Base
  belongs_to :poll
  has_many :options, dependent: :destroy
  has_many :results_questions, dependent: :destroy

  def data_yes_or_not(lang)
    attributes = lang.eql?("en") ? ["Yes", "No"] : ["Si", "No"]
    {
      labels: attributes,
      datasets: [
        {
          backgroundColor: ["#5FB404", "#FF0040"],
          borderColor: "rgba(220,220,220,1)",
          data: [
            self.results_questions.where(option: attributes.first).count,
            self.results_questions.where(option: attributes.last).count
          ]
        }
      ]
    }
  end

  def data_selections
    option_names = self.options.map { |o| o.name }
    colors = self.options.map { |o| "##{SecureRandom.hex(3)}" }
    data = self.results_questions.group_by(&:option).map { |r| r.second.count }
    {
      labels: option_names,
      datasets: [
        {
          backgroundColor: colors,
          borderColor: "rgba(220,220,220,1)",
          data: data
        }
      ]
    }
  end
end
