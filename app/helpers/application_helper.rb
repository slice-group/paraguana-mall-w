module ApplicationHelper
	def title(page_title)
		content_for(:title) { page_title }
	end

	def	meta_description(page_description)
		content_for(:description) { page_description }
	end

	def loggedin?
		current_user
	end

	def is_index?
		action_name.to_sym.eql?(:index)
	end

	def model
		controller.controller_path.classify.constantize
	end

	def header_information(&block)
		content_for(:header_information) { capture(&block) }
	end

	def en_link
    form_locale_url("en")
	end

  def es_link
  	form_locale_url("es")
  end

  def form_locale_url(locale)
  	if params[:locale]
	  		request.url.gsub("/#{params[:locale]}/", "/#{locale}/")
	  else
	  		"#{request.base_url}/#{locale}#{request.fullpath}"
  	end
  end

  def locale?(languaje)
  	I18n.locale.to_sym.eql?(languaje.to_sym)
  end

  def domain?(domain)
  	request.domain.split('.').last.to_sym.eql?(domain)
  end
end
