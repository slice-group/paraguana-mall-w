module IndexHelper
  def entries(total, objects)
    unless total.zero?
      content_tag :div, class: "entries hidden-xs" do
        if objects.first_page?
          from, to = 1, objects.size
        elsif objects.last_page?
          from, to = ((objects.current_page * objects.default_per_page) - objects.default_per_page) + 1, total
        else
          from = ((objects.current_page * objects.default_per_page) - objects.default_per_page) + 1
          to = objects.current_page * objects.default_per_page
        end
        t('helpers.entries', start: from, end: to, total: total)
      end
    end
  end
end