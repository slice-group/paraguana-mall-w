$(document).ready(function(){

  $('.datepicker').datepicker({
    dayViewHeaderFormat: 'YYYY',
    format: 'dd-mm-yyyy',
    startDate: 'm',
    autoclose: 'true',
    todayBtn: 'linked',
    language: 'es'
  });
});

function hideAlert() {
  $.when($('.alert').hide('slow')).done(function(){
    $('.alert').remove();
  });
}
