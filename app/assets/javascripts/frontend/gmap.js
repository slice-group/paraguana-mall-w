var map;
var markersArray = [];
var latlng = new google.maps.LatLng(11.6792925,-70.1524258);

function initialize()
{
  var myOptions = {
      zoom: 16,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: false
  };

  map = new google.maps.Map(document.getElementById("map"), myOptions);
  placeMarker(latlng, map, "Autopista Intercomunal Coro-Punto Fijo, Sector El Cardón, C.C. Paraguana Mall., Punto Fijo, Península de Paraguaná 4102, Venezuela");
}

function placeMarker(location, map, title) {
  var marker = new google.maps.Marker({
      position: location,
      map: map,
      animation: google.maps.Animation.BOUNCE,
      title: title,
      icon: "/assets/frontend/favicon.png"
  });
  // add marker in markers array
  markersArray.push(marker);
}


google.maps.event.addDomListener(window, 'load', initialize);
