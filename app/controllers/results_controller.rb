#Generado con Keppler.
class ResultsController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_result, only: [:show, :edit, :update, :destroy]
  before_action :set_poll

  # GET /results
  def index
    results = Result.searching(@query).where(poll_id: @poll.id)
    @objects, @total = results.page(@current_page), results.size
    redirect_to results_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /results/1
  def show
  end

  # GET /results/new
  def new
    @result = Result.new
  end

  # GET /results/1/edit
  def edit
  end

  # POST /results
  def create
    @result = Result.new(result_params)
    @result.poll_id = @poll.id

    if @result.save
      redirect_to poll_results_path(@poll), notice: 'Result was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /results/1
  def update
    if @result.update(result_params)
      redirect_to poll_results_path(@poll), notice: 'Result was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /results/1
  def destroy
    @result.destroy
    redirect_to poll_results_path(@poll), notice: 'Result was successfully destroyed.'
  end

  def destroy_multiple
    Result.destroy redefine_ids(params[:multiple_ids])
    redirect_to poll_results_path(@poll, page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_result
      @result = Result.find(params[:id])
    end

    def set_poll
      @poll = Poll.find(params[:poll_id])
    end

    # Only allow a trusted parameter "white list" through.
    def result_params
      params.require(:result).permit(:firstname, :lastname, :email, :poll_id)
    end
end
