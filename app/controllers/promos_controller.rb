#Generado con Keppler.
class PromosController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_promo, only: [:show, :edit, :update, :destroy]

  # GET /promos
  def index
    promos = Promo.searching(@query).all
    @objects, @total = promos.page(@current_page), promos.size
    redirect_to promos_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /promos/1
  def show
  end

  # GET /promos/new
  def new
    @promo = Promo.new
  end

  # GET /promos/1/edit
  def edit
  end

  # POST /promos
  def create
    @promo = Promo.new(promo_params)

    if @promo.save
      redirect_to @promo, notice: 'Promo was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /promos/1
  def update
    if @promo.update(promo_params)
      redirect_to promos_path, notice: 'Promo was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /promos/1
  def destroy
    @promo.destroy
    redirect_to promos_url, notice: 'Promo was successfully destroyed.'
  end

  def destroy_multiple
    Promo.destroy redefine_ids(params[:multiple_ids])
    redirect_to promos_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_promo
      @promo = Promo.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def promo_params
      params.require(:promo).permit(:image, :name, :public)
    end
end
