#Generado con Keppler.
class EventImagesController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_event_image, only: [:show, :edit, :update, :destroy]

  # GET /event_images
  def index
    event_images = EventImage.searching(@query).all
    @objects, @total = event_images.page(@current_page), event_images.size
    redirect_to event_images_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /event_images/1
  def show
  end

  # GET /event_images/new
  def new
    if EventImage.all.count >= 2
      redirect_to event_images_path, notice: 'No puede tener mas de dos imágenes.'
    else
      @event_image = EventImage.new
    end
  end

  # GET /event_images/1/edit
  def edit
  end

  # POST /event_images
  def create
    @event_image = EventImage.new(event_image_params)
    if @event_image.save
      redirect_to @event_image, notice: 'Event image was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /event_images/1
  def update
    if @event_image.update(event_image_params)
      redirect_to @event_image, notice: 'Event image was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /event_images/1
  def destroy
    @event_image.destroy
    redirect_to event_images_url, notice: 'Event image was successfully destroyed.'
  end

  def destroy_multiple
    EventImage.destroy redefine_ids(params[:multiple_ids])
    redirect_to event_images_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event_image
      @event_image = EventImage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def event_image_params
      params.require(:event_image).permit(:name, :image)
    end
end
