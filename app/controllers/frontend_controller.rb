class FrontendController < ApplicationController
  layout 'layouts/frontend/application'
  before_action :set_urls

  def index
    @rooms = Room.with_translations(params[:locale] || 'es')
    @rooms = @rooms.reject { |room| room.images.empty? }
    @promo = Promo.find_by public: true
  end

  def paraguana
  end

  def facilities
  end

  def show_room
    @modal_opened = true if params[:modal_opened]
    @room = Room.find_by_permalink params[:permalink]

    respond_to do |format|
      format.js
    end
  end

  def poll
    lang = params[:locale].eql?("en") ? "en" : "es"
    @poll = Poll.find_by_language(lang)
  end

  def send_poll
    result_info = Result.create(build_result)
    results = params.to_a[5..(params.to_a.count-5)]
    results.each do |result|
      build_option(result_info, result)
    end
    redirect_to root_path
  end

  private
  def set_urls
    @reserva = 'http://paraguanamallhotel.com/booking'
    @twitter = 'https://twitter.com/pm_hotel'
    @tripadvisor = 'https://www.tripadvisor.com.ve/Hotel_Review-g316095-d5789162-Reviews-Paraguana_Mall_Hotel-Punto_Fijo_Paraguana_Peninsula_Central_Western_Region.html'
    @facebook = 'https://www.facebook.com/Paraguan%C3%A1-Mall-Hotel-162360603915332'
    @instagram = 'https://www.instagram.com/paraguanamallhotel/'
  end

  def build_result
    {
      firstname: params[:firstname],
      lastname: params[:lastname],
      email: params[:email],
      poll_id: Poll.where(public: true).first.id
    }
  end

  def build_option(result_info, result)
    if result.second.class == ActiveSupport::HashWithIndifferentAccess
      option = result.to_a.second
      if option.first.first.eql?("answer")
        add_result(option.first.second, result_info.id, result.first)
      else
        option.each do |item|
          add_result(item.first, result_info.id, result.first) if !item.eql?("on")
        end
      end
    else
      add_result(result.second, result_info.id, result.first)
    end
  end

  def add_result(option, result_id, question_id)
    ResultsQuestion.create(option: option, result_id: result_id, question_id: question_id)
  end
end
