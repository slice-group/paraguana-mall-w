#Generado con Keppler.
class PollsController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource only: [:index, :show, :edit, :update, :destroy, :destroy_multiple]
  before_action :set_poll, only: [:show, :edit, :update, :destroy]
  before_action :set_poll_in_statistics, only: [:poll_statistics]

  # GET /polls
  def index
    polls = Poll.searching(@query).all
    @objects, @total = polls.page(@current_page), polls.size
    redirect_to polls_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /polls/1
  def show
    @questions = Question.where(poll_id: @poll.id)
  end

  # GET /polls/new
  def new
    @poll = Poll.new
  end

  # GET /polls/1/edit
  def edit
  end

  # POST /polls
  def create
    @poll = Poll.new(poll_params)

    if @poll.save
      redirect_to @poll, notice: 'Poll was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /polls/1
  def update
    if @poll.update(poll_params)
      redirect_to @poll, notice: 'Poll was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /polls/1
  def destroy
    @poll.destroy
    redirect_to polls_url, notice: 'Poll was successfully destroyed.'
  end

  def destroy_multiple
    Poll.destroy redefine_ids(params[:multiple_ids])
    redirect_to polls_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente"
  end

  def poll_statistics
    @questions = Question.where(poll_id: @poll.id)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_poll
      @poll = Poll.find(params[:id])
    end

    def set_poll_in_statistics
      @poll = Poll.find(params[:poll_id])
    end

    # Only allow a trusted parameter "white list" through.
    def poll_params
      params.require(:poll).permit(:name, :public)
    end
end
