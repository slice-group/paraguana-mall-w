class QuestionsController < ApplicationController
  layout 'admin/application'
  before_action :set_poll
  before_action :set_question, only: [:delete, :edit, :add_option, :update, :delete_option]

  def create
    @question_type = params[:question_type]
    count = Question.count
    @questions = Question.where(poll_id: @poll.id)
    Question.create(
      sentence: "<insertar pregunta>",
      description: "<insertar descripción>",
      ask_type: @question_type,
      position: count+1,
      poll_id: @poll.id
    )
  end

  def delete
    @question_type = params[:question_type]
    @questions = Question.where(poll_id: @poll.id)
    @question.results_questions.destroy
    @question.destroy
  end

  def edit
  end

  def update
    @question.update(question_params)
  end

  def add_option
    Option.create name: params[:name], question_id: @question.id
  end

  def delete_option
    @option = Option.find(params[:option_id])
    @option.destroy
  end

  private

  def set_question
    @question = Question.find(params[:id])
  end

  def set_poll
    @poll = Poll.find(params[:poll_id])
  end

  def question_params
    params.require(:question).permit(:sentence, :description)
  end

end
