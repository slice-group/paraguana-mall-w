class EventMailer < ApplicationMailer
  default from: "mercadeoyventas@paraguanamallhotel.com"

  def event_admin(evento)
    @evento = evento
    mail(
      to: 'mercadeoyventas@paraguanamallhotel.com',
      subject: "#{evento.client.name.humanize} ha enviado una solicitud de Evento."
      )
  end

  def event_user(evento)
    @evento = evento
    mail(
      to: @evento.client.email,
      subject: "Has enviado una solicitud a Paraguana Mall Hotel."
      )
  end
end
