Rails.application.routes.draw do

  scope "(:locale)", :locale => /en|es/ do
    root to: 'frontend#index'

    devise_for :users, skip: KepplerConfiguration.skip_module_devise

    resources :admin, only: :index

    get "/eventos" => 'events#new', :as => :new_event
    post "/eventos" => 'events#create', :as => :create_events
    get "/paraguana" => "frontend#paraguana" , as: :paraguana
    get "/instalaciones" => "frontend#facilities" , as: :facilities
    get "/room/:permalink" => 'frontend#show_room' , as: :show_room
    get "/encuesta" => 'frontend#poll', :as => :new_poll
    post "/enviar_encuesta" => 'frontend#send_poll', :as => :send_poll

    scope :admin do
      resources :polls do
        get '/statistics', to: "polls#poll_statistics", as: :statistics
        get '/question/create/:question_type', to: "questions#create", as: :question_create
        delete '/question/delete/:id', to: "questions#delete", as: :question_delete
        get '/question/edit/:id/', to: "questions#edit", as: :question_edit
        delete '/question/:id/delete_option/:option_id', to: "questions#delete_option", as: :question_delete_option
        post '/question/:id/add_option/', to: "questions#add_option", as: :question_add_option
        patch '/question/update/:id/', to: "questions#update", as: :question
        get '(page/:page)', action: :index, on: :collection, as: ''
        delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple

        resources :results do
          get '(page/:page)', action: :index, on: :collection, as: ''
          delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
        end
      end

    	resources :users do
        get '(page/:page)', action: :index, on: :collection, as: ''
        delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
      end

      resources :rooms do
        get '(page/:page)', action: :index, on: :collection, as: ''
        delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
      end

      get 'clients/export' => 'clients#export', as: :export_clients
      resources :clients do
        get '(page/:page)', action: :index, on: :collection, as: ''
        delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
      end

      resources :events, except: [:new, :create] do
        get '(page/:page)', action: :index, on: :collection, as: ''
        get '/export', action: :export, as: :export
        delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
      end

      resources :promos do
        get '(page/:page)', action: :index, on: :collection, as: ''
        delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
      end

      resources :event_images do
        get '(page/:page)', action: :index, on: :collection, as: ''
        delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
      end
    end


    #errors
    match '/403', to: 'errors#not_authorized', via: :all, as: :not_authorized
    match '/404', to: 'errors#not_found', via: :all
    match '/422', to: 'errors#unprocessable', via: :all
    match '/500', to: 'errors#internal_server_error', via: :all


    #dashboard
    mount KepplerGaDashboard::Engine, :at => '/', as: 'dashboard'
  end

end
