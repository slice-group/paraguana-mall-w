namespace :patches do
	desc "TODO"

	task create_admin: :environment do
    user = User.new name: "Admin", email: "admin@keppler.com", password: "12345678", password_confirmation: "12345678", role_ids: "1"
    puts "admin@keppler.com ha sido creado" if user.save
	end

	task create_poll: :environment do
		[:es, :en].each do |lang|
		  Poll.create name: "Poll (#{lang})", language: lang, public: true
		end
	end

end
