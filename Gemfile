source 'https://rubygems.org'

gem 'rails', '4.2.0'

#javascript
gem 'jquery-rails'
gem 'angularjs-rails'
gem 'nprogress-rails'
gem "ckeditor"
gem 'mini_magick'

gem "jquery-slick-rails"

#translate
gem 'globalize', '~> 5.0.0'

#documentation
gem 'sdoc', '~> 0.4.0',          group: :doc

#utilities
gem 'haml-rails'
gem 'coffee-rails', '~> 4.0.0'
gem 'sass-rails'
gem 'therubyracer', '~> 0.11.4', platforms: :ruby
gem 'uglifier', '>= 1.3.0'
gem 'jbuilder', '~> 2.0'
gem 'turbolinks'
gem 'jquery-turbolinks'
gem 'bootstrap-datepicker-rails'
gem "combined_time_select", "~> 1.0.1"
gem 'sanitize'

#database
gem 'mysql2', '~> 0.3.18'

#SEO
gem 'sitemap_generator'

#websocket
#gem 'faye'
#gem 'thin'
#gem 'sync'
gem 'chartjs-ror'

#forms
gem 'simple_form'
gem 'cocoon'

# Upload
gem 'carrierwave'
gem 'rmagick'

#desgin
gem 'bourbon'
gem 'bootstrap-sass'
gem 'font-awesome-sass', '~> 4.6.0'
gem 'material_icons'
gem 'materialize-sass'

#authentication and authorization
gem 'devise'
gem 'devise-i18n'
gem 'cancancan'
gem 'rolify'

#elasticsearch
gem 'elasticsearch-model'
gem 'elasticsearch-rails'

#pagination
gem 'kaminari-i18n'
gem 'kaminari'

#exportation xlsx
gem 'rubyzip', '~> 1.0.0'
gem 'axlsx', '~> 2.0.1'
gem 'axlsx_rails', '~> 0.2.0'

#exportation pdf
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'

#development
group :development, :test do
  gem 'better_errors'
  gem 'quiet_assets'
  gem 'rails_layout'
  gem 'table_print'
  gem 'spring'
  gem 'guard'
  #gem 'guard-sass'
  gem 'guard-coffeescript'
  gem 'guard-livereload'
  gem 'byebug'
  gem 'web-console', '~> 2.0'
  gem 'commands'
  # Style guides
  gem 'rubocop', require: false
  gem 'haml-lint', require: false
  gem 'scss_lint', require: false
  gem 'letter_opener'
end

#modules
#---- keppler_ga_dashboard ----
gem 'keppler_ga_dashboard', git: 'https://github.com/inyxtech/keppler_ga_dashboard.git', tag: '1.0.0'
gem 'google-api-client', require: 'google/api_client'
