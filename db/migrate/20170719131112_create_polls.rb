class CreatePolls < ActiveRecord::Migration
  def change
    create_table :polls do |t|
      t.string :name
      t.string :language
      t.boolean :public, default: false

      t.timestamps null: false
    end
  end
end
