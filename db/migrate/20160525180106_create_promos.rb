class CreatePromos < ActiveRecord::Migration
  def change
    create_table :promos do |t|
      t.string :image
      t.string :name
      t.boolean :public

      t.timestamps null: false
    end
  end
end
