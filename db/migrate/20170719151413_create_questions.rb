class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :sentence
      t.text :description
      t.string :ask_type
      t.integer :position
      t.integer :poll_id

      t.timestamps null: false
    end
  end
end
