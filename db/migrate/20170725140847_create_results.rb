class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.string :poll_id

      t.timestamps null: false
    end
  end
end
