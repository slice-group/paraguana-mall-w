class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :name
      t.float :price
      t.text :description
      t.string :permalink

      t.timestamps null: false
    end
  end
end
