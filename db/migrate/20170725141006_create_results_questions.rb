class CreateResultsQuestions < ActiveRecord::Migration
  def change
    create_table :results_questions do |t|
      t.string :option
      t.integer :result_id
      t.integer :question_id

      t.timestamps null: false
    end
  end
end
