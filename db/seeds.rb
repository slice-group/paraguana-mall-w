# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#user = CreateAdminService.new.call
#puts 'CREATED ADMIN USER: ' << user.email

[:admin, :reception, :events].each do |name|
  rol = Role.new name: name
  puts "#{name} creado" if rol.save
end

[:es, :en].each do |lang|
  Poll.create name: "Poll (#{lang})", language: lang, public: true
end

['Salón 1', 'Salón 2'].each do |name|
  img = EventImage.new name: name
  puts "#{name} creado" if img.save
end

user = User.new name: "Admin", email: "admin@keppler.com", password: "12345678", password_confirmation: "12345678", role_ids: "1"
puts "admin@keppler.com ha sido creado" if user.save

['Standard', 'Doble', 'Junior suite', 'Suite'].each do |name|
  room = Room.new name: name,
                  price: '0',
                  description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
  puts "Habitacion #{name} creado" if room.save
end
